[package]
# --- Bumpversion match - do not reorder
name = "veilid-tools"
version = "0.2.1"
# ---
description = "A collection of baseline tools for Rust development use by Veilid and Veilid-enabled Rust applications"
authors = ["Veilid Team <contact@veilid.com>"]
license = "MPL-2.0"
edition = "2021"

[lib]
# staticlib for iOS tests, cydlib for android tests, rlib for everything else
crate-type = ["cdylib", "staticlib", "rlib"]

[features]
default = ["rt-tokio"]
rt-async-std = ["async-std", "async_executors/async_std"]
rt-tokio = [
    "tokio",
    "tokio-util",
    "async_executors/tokio_tp",
    "async_executors/tokio_io",
    "async_executors/tokio_timer",
]
rt-wasm-bindgen = ["async_executors/bindgen", "async_executors/timer"]

veilid_tools_android_tests = ["dep:paranoid-android"]
veilid_tools_ios_tests = ["dep:oslog", "dep:tracing-oslog"]
tracing = ["dep:tracing", "dep:tracing-subscriber"]
network-result-extra = []
network-result-info = []

[dependencies]
tracing = { version = "0.1.37", features = [
    "log",
    "attributes",
], optional = true }
tracing-subscriber = { version = "0.3.17", optional = true }
log = { version = "0.4.20" }
eyre = "0.6.8"
static_assertions = "1.1.0"
cfg-if = "1.0.0"
thiserror = "1.0.47"
futures-util = { version = "0.3.28", default_features = false, features = [
    "alloc",
] }
parking_lot = "0.12.1"
once_cell = "1.18.0"
stop-token = { version = "0.7.0", default-features = false }
rand = "0.8.5"
rand_core = "0.6.4"
backtrace = "0.3.68"
fn_name = "0.1.0"
range-set-blaze = "0.1.9"
flume = { version = "0.11.0", features = ["async"] }

# Dependencies for native builds only
# Linux, Windows, Mac, iOS, Android
[target.'cfg(not(target_arch = "wasm32"))'.dependencies]
async-std = { version = "1.12.0", features = ["unstable"], optional = true }
tokio = { version = "1.32.0", features = ["full"], optional = true }
tokio-util = { version = "0.7.8", features = ["compat"], optional = true }
futures-util = { version = "0.3.28", default-features = false, features = [
    "async-await",
    "sink",
    "std",
    "io",
] }
chrono = "0.4.26"

libc = "0.2.147"
nix = "0.26.2"

# Dependencies for WASM builds only
[target.'cfg(target_arch = "wasm32")'.dependencies]
wasm-bindgen = "0.2.87"
js-sys = "0.3.64"
wasm-bindgen-futures = "0.4.37"
async_executors = { version = "0.7.0", default-features = false }
getrandom = { version = "0.2", features = ["js"] }

async-lock = "2.8.0"
send_wrapper = { version = "0.6.0", features = ["futures"] }

# Dependencies for Android
[target.'cfg(target_os = "android")'.dependencies]
jni = "0.21.1"
jni-sys = "0.3.0"
ndk = { version = "0.7.0" }
ndk-glue = { version = "0.7.0", features = ["logger"] }
lazy_static = "1.4.0"
paranoid-android = { version = "0.2.1", optional = true }
android_logger = "0.13.3"

# Dependencies for Windows
# [target.'cfg(target_os = "windows")'.dependencies]
# windows = { version = "^0", features = [ "Win32_NetworkManagement_Dns", "Win32_Foundation", "alloc" ]}
# windows-permissions = "^0"

# Dependencies for iOS
[target.'cfg(target_os = "ios")'.dependencies]
oslog = { version = "0.2.0", optional = true }
tracing-oslog = { version = "0.1.2", optional = true }

### DEV DEPENDENCIES

[dev-dependencies]
serial_test = "^2.0.0"
simplelog = { version = "0.12.1", features = ["test"] }

[target.'cfg(target_arch = "wasm32")'.dev-dependencies]
console_error_panic_hook = "0.1.7"
wasm-bindgen-test = "0.3.37"
wee_alloc = "0.4.5"
wasm-logger = "0.2.0"
tracing-wasm = { version = "0.2.1" }

### BUILD OPTIONS

[package.metadata.wasm-pack.profile.release]
wasm-opt = ["-O", "--enable-mutable-globals"]

[package.metadata.ios]
build_targets = [
    "aarch64-apple-ios",
    "aarch64-apple-ios-sim",
    "x86_64-apple-ios",
]
deployment_target = "12.0"
build_id_prefix = "com.veilid.veilidtools"
